import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { KontaktPage} from "../pages/kontakt/kontakt";
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuslandPage} from "../pages/ausland/ausland";
import { OsterreichPage } from "../pages/osterreich/osterreich";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServicesPage } from "../pages/services/services";
import { TarifPage } from "../pages/tarif/tarif";
import { UberPage } from "../pages/uber/uber";
import { WienPage } from "../pages/wien/wien";
import { FormPage} from "../pages/form/form";
import {AusnahmePage} from "../pages/ausnahme/ausnahme";
import {PauschlPage} from "../pages/pauschl/pauschl";
import { HttpModule } from '@angular/http';
import { Toast } from '@ionic-native/toast';
import {FormAuslandPage} from "../pages/form-ausland/form-ausland";
import {FormAusnahmePage} from "../pages/form-ausnahme/form-ausnahme";
import {FormOsterPage} from "../pages/form-oster/form-oster";
import {FormPausPage} from "../pages/form-paus/form-paus";
import {FormUnserePage} from "../pages/form-unsere/form-unsere";
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AuslandPage,
    KontaktPage,
    OsterreichPage,
    ServicesPage,
    TarifPage,
    UberPage,
    WienPage,
    FormPage,
    FormAusnahmePage,
    FormOsterPage,
    FormPausPage,
    FormUnserePage,
    AusnahmePage,
    PauschlPage,
    FormAuslandPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AuslandPage,
    KontaktPage,
    OsterreichPage,
    ServicesPage,
    TarifPage,
    FormAusnahmePage,
    FormOsterPage,
    FormPausPage,
    FormUnserePage,
    UberPage,
    WienPage,
    FormAuslandPage,
    FormPage,
    AusnahmePage,
    PauschlPage
  ],
  providers: [
    StatusBar,
    Toast,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
