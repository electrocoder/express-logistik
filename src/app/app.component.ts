import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { ServicesPage } from "../pages/services/services";
import { TarifPage } from "../pages/tarif/tarif";
import { UberPage } from "../pages/uber/uber";
import { WienPage } from "../pages/wien/wien";
import { AuslandPage} from "../pages/ausland/ausland";
import { OsterreichPage } from "../pages/osterreich/osterreich";
import { KontaktPage} from "../pages/kontakt/kontakt";
import {PauschlPage} from "../pages/pauschl/pauschl";
import {AusnahmePage} from "../pages/ausnahme/ausnahme";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Für Wien', component: WienPage },
      { title: 'Für Österreich', component: OsterreichPage },
      { title: 'Für Ausland', component: AuslandPage },
      { title: 'Pauschl', component: PauschlPage },
      { title: 'Ausnahme', component: AusnahmePage },
      { title: 'Tarif Paket', component: TarifPage },
      { title: 'Über uns', component: UberPage },
      { title: 'Services', component: ServicesPage },
      { title: 'Kontakt', component: KontaktPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
