import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {

  services: string = "robert";
  referenz: string = "post";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
