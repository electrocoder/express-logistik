import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormPausPage} from "../form-paus/form-paus";



@Component({
  selector: 'page-pauschl',
  templateUrl: 'pauschl.html',
})
export class PauschlPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goForm(){

    this.navCtrl.push(FormPausPage);
  }

}
