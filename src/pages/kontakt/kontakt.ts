import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Headers, Http, RequestOptions} from "@angular/http";
import {Toast} from "@ionic-native/toast";

@Component({
  selector: 'page-kontakt',
  templateUrl: 'kontakt.html',
})
export class KontaktPage {

  url = "http://www.express-logistik.at/wp-json/contact-form-7/v1/contact-forms/3592/feedback";
  vorname:any;
  nachname:any;
  phone:any;
  email:any;
  message:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private toast: Toast,private http: Http) {
  }

  static ObjecttoParams(obj)
  {
    var p = [];
    for (var key in obj)
    {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };

  post(){
    var headers = new Headers();
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let dataSon={

      "conference-first-name":this.vorname,
      "conference-last-name":this.nachname,
      "conference-phone-number":this.phone,
      "conference-email":this.email,
      "message":this.message

    };

    this.http.post(this.url,
      KontaktPage.ObjecttoParams(dataSon), options)
      .subscribe(data => {

        this.toast.show("Ihre Nachricht erfolgreich gesendet.", '5000', 'bottom').subscribe(
          toast => {
            console.log(toast);
          }
        );
      }, error => {
        this.toast.show("Ihre Nachricht konnte nicht gesendet.", '5000', 'bottom').subscribe(
          toast => {
            console.log(toast);
          }
        );

      });
  }

}
