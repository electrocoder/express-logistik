import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormOsterPage} from "../form-oster/form-oster";


@Component({
  selector: 'page-osterreich',
  templateUrl: 'osterreich.html',
})
export class OsterreichPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }


  goForm(){

    this.navCtrl.push(FormOsterPage);
  }
}
