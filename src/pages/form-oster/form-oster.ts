import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import {Headers, Http, RequestOptions} from "@angular/http";
import {Toast} from "@ionic-native/toast";

@Component({
  selector: 'page-form-oster',
  templateUrl: 'form-oster.html',
})
export class FormOsterPage {

  url = "http://www.express-logistik.at/wp-json/contact-form-7/v1/contact-forms/6428/feedback";
  tarih:any;
  stunde:any;
  minuten:any;
  name:any;
  phone:any;
  packet="Für Österreich";
  product:any;
  von_strasse:any;
  von_haus:any;
  von_plz:any;
  stadt:any;
  von_land:any;
  nach_strasse:any;
  nach_haus:any;
  nach_plz:any;
  nach_stadt:any;
  nach_land:any;
  email:any;
  note:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,private http: Http,private toast: Toast) {
  }

  static ObjecttoParams(obj)
  {
    var p = [];
    for (var key in obj)
    {
      p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
  };


  post(){
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8' );
    let options = new RequestOptions({ headers: headers });
    let dataSon = {
      tarih:this.tarih,
      stunde:this.stunde,
      minuten:this.minuten,
      name:this.name,
      phone:this.phone,
      packet:this.packet,
      product:this.product,
      von_strasse:this.von_strasse,
      von_haus:this.von_haus,
      von_plz:this.von_plz,
      stadt:this.stadt,
      von_land:this.von_land,
      nach_strasse:this.nach_strasse,
      nach_haus:this.nach_haus,
      nach_plz:this.nach_plz,
      nach_stadt:this.nach_stadt,
      nach_land:this.nach_land,
      email:this.email,
      note:this.note
    };
    this.http.post(this.url,
      FormOsterPage.ObjecttoParams(dataSon), options)
      .subscribe(data => {

        this.toast.show("Ihre Nachricht erfolgreich gesendet.", '5000', 'bottom').subscribe(
          toast => {
            console.log(toast);
          }
        );
      }, error => {
        this.toast.show("Ihre Nachricht konnte nicht gesendet.", '5000', 'bottom').subscribe(
          toast => {
            console.log(toast);
          }
        );

      });
  }
}
