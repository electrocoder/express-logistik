import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormUnserePage} from "../form-unsere/form-unsere";



@Component({
  selector: 'page-tarif',
  templateUrl: 'tarif.html',
})
export class TarifPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goForm(){

    this.navCtrl.push(FormUnserePage);
  }

}
