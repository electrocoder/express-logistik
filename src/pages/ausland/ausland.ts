import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FormAuslandPage} from "../form-ausland/form-ausland";


@Component({
  selector: 'page-ausland',
  templateUrl: 'ausland.html',
})
export class AuslandPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  goForm(){
    this.navCtrl.push(FormAuslandPage);
  }
}
