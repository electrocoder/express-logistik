import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {WienPage} from "../wien/wien";
import {OsterreichPage} from "../osterreich/osterreich";
import {PauschlPage} from "../pauschl/pauschl";
import {AusnahmePage} from "../ausnahme/ausnahme";
import {AuslandPage} from "../ausland/ausland";
import { SplashScreen } from '@ionic-native/splash-screen';
import {TarifPage} from "../tarif/tarif";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,private splashScreen: SplashScreen) {

  }

   ionViewDidLoad() {
    this.splashScreen.hide();
  }

  goPage(where){
    let page:any;
    switch(where) {
      case 1:
        page=WienPage;
        break;
      case 2:
        page=OsterreichPage;
        break;
      case 3:
        page=PauschlPage;
        break;
      case 4:
        page=AusnahmePage;
        break;
      case 5:
        page=AuslandPage;
        break;
      case 6:
        page=TarifPage;
        break;
    }

    this.navCtrl.push(page);
  }
}
